package com.geeswitch.geeswitch;

/**
 * Created by dbrosch on 15.11.17.
 */

public class GSwitch {

    String name;
    String ip;

    public GSwitch(String name, String ip){
        this.name = name;
        this.ip = ip;
    }

    public String getIP() {
        return ip;
    }

    public String getName() {
        return name;
    }
}
