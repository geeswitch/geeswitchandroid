package com.geeswitch.geeswitch;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

public class overview extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        GeeSwitch.startup(getApplication());
        setContentView(R.layout.activity_overview);
        final TextView debug = findViewById(R.id.debug);
        final Thread loader = new Thread(new Runnable() {
            @Override
            public void run() {
                GeeSwitch.getInstance().scanNetwork(debug);
                next();
            }
        });
        loader.start();
        final Button cancelButton = findViewById(R.id.cancelButton);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loader.interrupt();
                next();
            }
        });
    }

    private void next(){
        Intent intent = new Intent(overview.this, LampActivity.class);
        startActivity(intent);
    }
}
