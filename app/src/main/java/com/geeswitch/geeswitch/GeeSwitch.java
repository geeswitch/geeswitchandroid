package com.geeswitch.geeswitch;

import android.app.Application;
import android.content.Context;
import android.net.wifi.WifiManager;
import android.provider.Settings;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.TextView;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.InterfaceAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Enumeration;

import javax.xml.transform.sax.SAXSource;

/**
 * Created by dbrosch on 15.11.17.
 */

public class GeeSwitch {

    private static GeeSwitch instance;
    public static String switchIP = null;


    private Application app;
    public GeeSwitch(Application app){
        this.app = app;
        onStartup();
    }

    public ArrayList<GSwitch> switches = new ArrayList<>();
    File mainConfig;

    public void onStartup(){
        mainConfig = new File(app.getFilesDir(), "switch_storage.json");
    }

    public void scanNetwork(final TextView debug){
        int timeout = 250;
        String subnet = "192.168.0";
        String myip = "192.168.0.123";
        try{
            for (final Enumeration<NetworkInterface> interfaces = NetworkInterface.getNetworkInterfaces();
                    interfaces.hasMoreElements();
                    ) {
                final NetworkInterface cur = interfaces.nextElement();

                if (cur.isLoopback()) {
                    continue;
                }

                System.out.println("interface " + cur.getName());

                for (final InterfaceAddress addr : cur.getInterfaceAddresses()) {
                    final InetAddress inet_addr = addr.getAddress();

                    if (!(inet_addr instanceof Inet4Address)) {
                        continue;
                    }

                    System.out.println(
                            "  address: " + inet_addr.getHostAddress() +
                                    "/" + addr.getNetworkPrefixLength()
                    );

                    System.out.println(
                            "  broadcast address: " +
                                    addr.getBroadcast().getHostAddress()
                    );
                    myip = addr.getBroadcast().getHostAddress();
                }
            }
        }catch(Exception ex){
            ex.printStackTrace();
        }
        subnet = myip.substring(0, subnet.lastIndexOf(".")-1);
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        final String finalMyip = myip;
        debug.post(new Runnable() {
            @Override
            public void run() {
                debug.setText("MyIP: "+ finalMyip +"; ");
            }
        });
        subnet = "192.168.1";
        for (int i = 0; i < 256; i++)
        {
            final String host = subnet + "." + i;
            try {
                System.out.println("Pinging "+host+"...");
                if (InetAddress.getByName(host).isReachable(timeout)) {
                    debug.post(new Runnable() {
                        @Override
                        public void run() {
                            debug.setText(debug.getText()+"Reached "+host+"; ");
                        }
                    });

                    System.out.println(host + " is reachable!");
                    HttpURLConnection httpConn = (HttpURLConnection) new URL("http://"+host+"/sdfghjfdxy").openConnection();
                    if(httpConn.getResponseCode()==HttpURLConnection.HTTP_OK){
                        //JsonObject o = new JsonParser().parse(new InputStreamReader(httpConn.getInputStream())).getAsJsonObject();
                        //TODO read json data
                        // http://192.168.0.123/api?status=1/2
                        BufferedReader r = new BufferedReader(new InputStreamReader(httpConn.getInputStream()));
                        final StringBuilder b = new StringBuilder();
                        String line;
                        while((line = r.readLine())!=null){
                            b.append(line);
                            b.append("\n");
                        }
                        debug.post(new Runnable() {
                            @Override
                            public void run() {
                                debug.setText(debug.getText()+b.toString());
                            }
                        });
                        Thread.sleep(2000);
                        GeeSwitch.switchIP = host;
                        System.out.println("Registered and connected to GeeSwitch!");
                        return;
                    }
                }
            }catch (ConnectException ex){
                System.out.println("Failed to send HTTP-Request: "+ex.getMessage());
            }
            catch(UnknownHostException ex){
                System.out.println("Unknown host!");
            }catch (Exception e) {
                System.out.println("Failed to reach host "+host+". Exception was: "+e.getMessage()+" ("+e.getClass().getSimpleName()+")");
            }
        }
    }

    public void saveSwitches(){

    }


    /*
        STATIC METHODS
     */

    public static void startup(Application app){
        instance = new GeeSwitch(app);
    }

    public static ArrayList<GSwitch> getSwitches(){
        return instance.switches;
    }

    public static GeeSwitch getInstance(){
        return instance;
    }



    /*
    COPIED METHODS
     */

    private static String intToIP(int ipAddress) {
        String ret = String.format("%d.%d.%d.%d", (ipAddress & 0xff),
                (ipAddress >> 8 & 0xff), (ipAddress >> 16 & 0xff),
                (ipAddress >> 24 & 0xff));

        return ret;
    }
}
