package com.geeswitch.geeswitch;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class LampActivity extends AppCompatActivity {

    String ip = "192.168.0.1";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lamp);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LampActivity.this, AddActivity.class);
                startActivity(intent);
            }
        });

        final Button changeButton = (Button) findViewById(R.id.toggleButton);
        changeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try{
                            final HttpURLConnection httpConn = (HttpURLConnection) new URL("http://"+GeeSwitch.switchIP+"/toggle").openConnection();
                            if(httpConn.getResponseCode()==HttpURLConnection.HTTP_OK){
                                view.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        Snackbar.make(view, "Changed status of "+GeeSwitch.switchIP+"!", Snackbar.LENGTH_LONG)
                                                .setAction("Action", null).show();
                                    }
                                });
                            }else{
                                view.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        try {
                                            Snackbar.make(view, "Failed to connect: "+GeeSwitch.switchIP+" responded with code "+httpConn.getResponseCode()+"!", Snackbar.LENGTH_LONG)
                                                    .setAction("Action", null).show();
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                });
                            }
                        }catch (final Exception ex){
                            view.post(new Runnable() {
                                @Override
                                public void run() {
                                    Snackbar.make(view, "Failed to connect to "+GeeSwitch.switchIP+": "+ex.getClass().getSimpleName()+" with message "+ex.getMessage(), Snackbar.LENGTH_LONG)
                                            .setAction("Action", null).show();
                                }
                            });
                            //xamarin forms
                        }
                    }
                }).start();
            }
        });

        final EditText ipInput = (EditText) findViewById(R.id.ipInput);
        ipInput.setHint("IP of device");
        ipInput.setText(ip);
        Button ipChangeButton =  (Button) findViewById(R.id.ipChangeButton);
        ipChangeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ip = ipInput.getText().toString();
                try{
                    GeeSwitch.switchIP = ip;
                        Snackbar.make(view, "IP of Wohnzimmer 01 has been changed to "+GeeSwitch.switchIP+"!", Snackbar.LENGTH_LONG)
                                .setAction("Action", null).show();
                }catch (Exception ex){
                    Snackbar.make(view, "Failed to connect: "+ex.getClass().getSimpleName()+" with message "+ex.getMessage(), Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                }

            }
        });
    }

}
