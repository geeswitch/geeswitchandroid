package com.geeswitch.geeswitch;

import android.os.SystemClock;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

public class AddActivity extends AppCompatActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);

        final ConstraintLayout wifiDisplay = findViewById(R.id.wifiDisplay);
        final ProgressBar loadingWifi = (ProgressBar) findViewById(R.id.loadingWifi);
        final ConstraintLayout main = (ConstraintLayout)findViewById(R.id.main);
        loadingWifi.setVisibility(View.INVISIBLE);
        wifiDisplay.setVisibility(View.INVISIBLE);


        final EditText ipInput = (EditText) findViewById(R.id.ipInput);
        final EditText nameInput = (EditText) findViewById(R.id.nameInput);
        final EditText ssidInput = (EditText) findViewById(R.id.ssidInput);
        final EditText wpwInput = (EditText) findViewById(R.id.wpwInput);

        final Button continueButton = (Button) findViewById(R.id.continueButton);
        final Button addButton = (Button) findViewById(R.id.addButton);

        continueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!ipInput.getText().toString().isEmpty()) {
                    ipInput.setFocusable(false);
                    ipInput.setClickable(false);
                    continueButton.setVisibility(View.GONE);
                    loadingWifi.setVisibility(View.VISIBLE);
                    Runnable runnable = new Runnable() {
                        @Override
                        public void run() {
                                //Connect to Switch
                            try {
                                HttpURLConnection con = (HttpURLConnection) new URL("http://"+ipInput.getText().toString()+"/").openConnection();
                                if(con.getResponseCode()==HttpURLConnection.HTTP_OK){

                                    main.post(new Runnable() {
                                        @Override
                                        public void run() {
                                            wifiDisplay.setVisibility(View.VISIBLE);
                                            loadingWifi.setVisibility(View.INVISIBLE);
                                        }
                                    });
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    };
                    new Thread(runnable).start();
                } else {
                    Snackbar.make(view, "Please enter a valid IP-Address", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                }
            }
        });
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadingWifi.setVisibility(View.VISIBLE);
                addButton.setVisibility(View.GONE);
            }
        });
    }
}
